<?php

namespace App\Policies;

use App\Models\Buyer;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyerPolicy
{
    use HandlesAuthorization;

    //hum policies me aakr methods use kr rhe h joo controller me use kr rhe h middleware ke through
    //before() call hoga jb bhi hum policy use krenge tb
    //yeah method humesha true return krna chaiye
    //q ki agr true return kia too neeche ke function use nin krega direct return hoo jaega
    //or agr false return kia too neeche ke functions use ni krne dega mtlb agr me khud buyer hu too bhi mujhe uska access nai dega
    public function before(User $user, $ability)
    {
        if($user->isAdmin())
        {
            return true;
        }
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Buyer  $buyer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Buyer $buyer)
    {
        return $user->id == $buyer->id;
    }

        public function purchase(User $user, Buyer $buyer)
    {
        return $user->id == $buyer->id;
    }
}
