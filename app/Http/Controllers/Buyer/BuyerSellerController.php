<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Models\Buyer;
use Illuminate\Http\Request;

class BuyerSellerController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index');
    }

    public function index(Buyer $buyer){
        $sellers = $buyer->transactions()
                        ->with('products.seller')
                        ->get()
                        ->pluck('producs.seller')
                        ->unique();

        return $this->showAll($sellers);
    }
}
