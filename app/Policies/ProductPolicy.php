<?php

namespace App\Policies;

use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    //hum policies me aakr methods use kr rhe h joo controller me use kr rhe h middleware ke through
    //before() call hoga jb bhi hum policy use krenge tb
    //yeah method humesha true return krna chaiye
    //q ki agr true return kia too neeche ke function use nin krega direct return hoo jaega
    //or agr false return kia too neeche ke functions use ni krne dega mtlb agr me khud buyer hu too bhi mujhe uska access nai dega
    public function before(User $user, $ability)
    {
        if($user->isAdmin())
        {
            return true;
        }
    }
    public function addCategory(User $user, Product $product)    
    {
        return $user->id == $product->seller->id;
    }

    public function deleteCategory(User $user, Product $product)    
    {
        return $user->id == $product->seller->id;
    }
}
