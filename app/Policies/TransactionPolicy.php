<?php

namespace App\Policies;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if($user->isAdmin())
        {
            return true;
        }
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Transaction $transaction)
    {
        return $this->isBuyer($user, $transaction) || $this->isSeller($user, $transaction);
    }

    private function isBuyer(User $user, Transaction $transaction): bool
    {
        return $this->id == $transaction->buyer_id;
    }

    private function isSeller(User $user, Transaction $transaction): bool
    {
        return $this->id == $transaction->product->seller->id;
    }

}
