<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\Buyer;

class BuyersController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index','show');
        $this->middleware('scope:read-general')->only('show');
        $this->middleware('can:view.buyer')->only('show');
    }

    public function index(){
        $buyers = Buyer::has('transactions')->get();
        // return response()->json(['count' => $buyers->count(), 'data' => $buyers], 200);
        return $this->showAll($buyers);
    }

    public function show(Buyer $buyer)
    {
        // $buyer = Buyer::has('transactions')->findOrFail($id);
        // return response()->json(['data' => $buyer], 200);
        return $this->showOne($buyer);
    }
}
