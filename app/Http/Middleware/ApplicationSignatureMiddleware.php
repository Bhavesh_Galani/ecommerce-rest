<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApplicationSignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $header = 'X-name')
    {
        $response = $next($request); //this is after middleware mtlb hum kuch request krenge then voo middleware se controller me jaega response return krega n the $response me store krega
        $response->headers->set($header, config('app.name'));
        return $response;
    }
}
