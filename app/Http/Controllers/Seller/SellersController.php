<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Models\Seller;
use Illuminate\Http\Request;

class SellersController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index','show');
        $this->middleware('scope:read-general')->only('show');
    }

    public function index(){
        $sellers = Seller::has('products')->get();
        // return response()->json(['count' => $sellers->count(), 'data' => $sellers], 200);
        return $this->showAll($sellers);
    }

    public function show(Seller $seller)
    {
        // $seller = Seller::has('products')->findOrFail($id);
        // return response()->json(['data' => $seller], 200);
        return $this->showOne($seller);
    }
}
